﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PlayerGameStatusUI : MonoBehaviour
{

    public PlayerController playerController;

    [Header("Game Stats")]
    public GameObject panelBackground;
    public TextMeshProUGUI gameStatusAndTimer;
    public TextMeshProUGUI redTicketsCount;
    public Image redTicketsBar;
    public TextMeshProUGUI blueTicketsCount;
    public TextMeshProUGUI kills;
    public GameObject killPanel;
    public Image blueTicketsBar;

    // Update is called once per frame
    void Update()
    {
        if (!playerController.isLocalPlayer)
            return;
        if (SuperGameManager.singleton.gameStatus != GameStatus.GAME)
        {
            if (SuperGameManager.singleton.gameStatus == GameStatus.WARMUP)
                gameStatusAndTimer.text = "WARMUP";
            if (SuperGameManager.singleton.gameStatus == GameStatus.ENDGAME)
                gameStatusAndTimer.text = "FINISHED";
            panelBackground.SetActive(false);
        }
        else
        {
            UpdateUIGameDatas();
        }
    }

    public void UpdateUIGameDatas()
    {
        panelBackground.SetActive(true);

        int baseTickets = SuperGameManager.singleton.baseTickets;
        int redTickets = SuperGameManager.singleton.redTickets;
        int blueTickets = SuperGameManager.singleton.blueTickets;
        float timeleft = SuperGameManager.singleton.GetTimeLeft;
        blueTicketsCount.text = "[" + blueTickets + "]";
        redTicketsCount.text = "[" + redTickets + "]";
        redTicketsBar.fillAmount = (float)redTickets / baseTickets;
        blueTicketsBar.fillAmount = (float)blueTickets / baseTickets;
        gameStatusAndTimer.text = "" + Mathf.Round(timeleft) + "s";
    }

    public void AddKill(string newKill)
    {
        this.kills.text += "\n" + newKill;
        killPanel.SetActive(true);
        Invoke(nameof(hideKillList), 3);
    }

    public void hideKillList()
    {
        this.kills.text = "";
        killPanel.SetActive(false);
    }
}
