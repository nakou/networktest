﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

/*
 * An explaination of the Log/Chat/Kill transmission of informations :
 * 
 * Remember first that, even if it's a Singleton, ChatAndLogManager exist on the client and on the server.
 * Therefore, it will serve as a platform to transmit informations client -> server -> clientS.
 * 
 * Each client, when joining the game, is referenced by the localPlayer value of SuperGameManager (which is also a Singleton and 
 * follow the same principes).
 * 
 * So, if you want to send a message to all player (log, chat or kill), you have to go through a server instruction.
 * If you want to call a [Server] method, they should be called by a player via a [Command] which execute the piece
 * of code on the server. And since you're on the server, you can call the [Server] methode under.
 * 
 * To allow clients to update their datas, the [Server] methods call their respective [Rpc] that, when executed on the clients,
 * can update UI element via the player prefabs.
 * 
 * NB : I could have those exchanges of informations place under the player prefab classes. But I wanted a specific place where everything
 * was happening. Also, since you're sending only one [Rpc] back to the client, instead of one [Rpc] per player prefab present on each client,
 * I do believe this is an efficient way to transfer informations and avoid waste (since [Rpc] on player other than the localPlayer will
 * be, at the end, ignored because you dont need to update inactive UIs).
 */
public class ChatAndLogManager : NetworkBehaviour
{
    public static ChatAndLogManager singleton;

    public void Awake()
    {
        InitializeSingleton();
    }

    void InitializeSingleton()
    {
        Debug.Log("Initialize Singleton");
        if (singleton != null && singleton == this)
        {
            return;
        }
        singleton = this;
    }

    [Server]
    public void Logger(string logContent)
    {
        AddLog(logContent);
        Debug.Log(logContent);
    }

    [Server]
    public void AddLog(string value)
    {
        if (value.Equals(""))
            return;
        value = "[" + DateTime.Now + "]" + value;
        if (ServerPlayer.singleton != null) // In case we're in the Player as server situation
            ServerPlayer.singleton.AddLog(value);
        RpcOnLogAdded(value);
    }

    [ClientRpc]
    public void RpcOnLogAdded(string value)
    {
        if (hasLocalPlayerInContext())
        {
            SuperGameManager.singleton.localPlayer.playerHUDController.UpdateLog(value);
        }
    }

    [Server]
    public void ChatAdded(string playerName, string message)
    {
        if (message.Equals(""))
            return;
        string value = ">" + playerName + " : " + message;
        if (ServerPlayer.singleton != null) // In case we're in the Player as server situation
            ServerPlayer.singleton.AddLog(value);
        RpcOnChatAdded(value);
    }

    [ClientRpc]
    public void RpcOnChatAdded(string value)
    {
        if (hasLocalPlayerInContext())
        {
            SuperGameManager.singleton.localPlayer.playerHUDController.UpdateChat(value);
        }
    }

    [Server]
    public void KillAdded(string killer, string killed)
    {
        string value = "";
        if (killer.Equals(killed))
        {
            value = "" + killer + " is no more...";
        }
        else
        {
            value = "" + killer + " killed " + killed;
        }
        if(ServerPlayer.singleton != null) // In case we're in the Player as server situation
            ServerPlayer.singleton.AddLog(value);
        RpcOnKillAdded(value); 
    }

    [ClientRpc]
    public void RpcOnKillAdded(string value)
    {
        if (hasLocalPlayerInContext())
        {
            SuperGameManager.singleton.localPlayer.playerHUDController.UpdateKill(value);
        }
    }

    private bool hasLocalPlayerInContext()
    {
        return SuperGameManager.singleton.localPlayer != null;
    }
}
