﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerVisualsController : NetworkBehaviour
{
    public GameObject playerBody;
    public GameObject aliveCam;
    public PlayerController playerController;

    public AudioListener audioListener;
    public Rigidbody rigidB;
    public GameObject deathCam;

    public Material red;
    public Material blue;
    public Renderer flag;

    public GameObject deadTankObject;

    public GameObject fireSmoke;

    [SyncVar(hook = nameof(OnShooting))] public bool Shoot;
    private float fireCount = 0.25f;
    private float fireCountTemp = 0;

    public void Update()
    {
        if (isServer && Shoot)
        {
            if(fireCountTemp >= fireCount)
            {
                Shoot = false;
                fireCountTemp = 0;
            }
            fireCountTemp += Time.deltaTime;
        }
    }

    public override void OnStartLocalPlayer()
    {
        audioListener.enabled = true;
        deathCam.SetActive(true);
    }

    public void setFlagColor()
    {
        if(playerController.playerInformations.team == Team.BLUE)
        {
            flag.material = blue;
            return;
        }
        if (playerController.playerInformations.team == Team.RED)
        {
            flag.material = red;
            return;
        }
    }

    public void OnShooting(bool value)
    {
        fireSmoke.SetActive(value);
    }

    [Command]
    public void CmdSpawnCadaver()
    {
        GameObject b = Instantiate(deadTankObject, transform.position, transform.rotation);
        NetworkServer.Spawn(b);
    }

}
