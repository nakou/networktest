﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class ItemSpawner : NetworkBehaviour
{
    float animationSpeed = 100;
    float respawnTimer = 30;
    float respawnTimerTemp = 0;
    [SyncVar(hook = nameof(OnStatusIsActive))] bool isActive = true;
    [Header("Item Configuration")]
    public ItemType itemType;
    public GameObject mainModel;

    [Header("Item Graphics")]
    
    public GameObject healModel;
    public GameObject ammoModel;

    public void Start()
    {
        switch (itemType)
        {
            case ItemType.AMMO:
                ammoModel.SetActive(true);
                healModel.SetActive(false);
                break;
            case ItemType.HEAL:
                ammoModel.SetActive(false);
                healModel.SetActive(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            mainModel.transform.Rotate(new Vector3(0, animationSpeed * Time.deltaTime, 0));
        }
        else
        {
            if (isServer)
            {
                if (respawnTimerTemp >= respawnTimer)
                {
                    respawnTimerTemp = 0;
                    isActive = true;
                }
                else
                    respawnTimerTemp += Time.deltaTime;
            }
        }
        
    }

    void OnStatusIsActive(bool activeStatus)
    {
        mainModel.SetActive(activeStatus);
    }

    // ServerCallback because we don't want a warning if OnTriggerEnter is called on the client
    [ServerCallback]
    private void OnCollisionEnter(Collision collision)
    {
        ColisionDetected(collision.gameObject);
    }

    // ServerCallback because we don't want a warning if OnTriggerEnter is called on the client
    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        ColisionDetected(other.gameObject);
    }

    void ColisionDetected(GameObject go)
    {
        if (isServer && isActive)
        {
            PlayerInformations informations = go.GetComponentInParent<PlayerInformations>();
            if (informations != null)
            {
                switch (itemType)
                {
                    case ItemType.AMMO:
                        informations.Reload(5);
                        break;
                    case ItemType.HEAL:
                        informations.Heal(5);
                        break;
                }
                isActive = false;
            }
        }
    }
}
