﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;

public class PlayerInformations : NetworkBehaviour
{
    [Header("PlayerVars")]
    [SyncVar] public Team team = Team.OBSERVER;
    [SyncVar(hook = nameof(OnPseudoChanged))] public string pseudo = "Client ";
    [SyncVar(hook = nameof(OnHealthChanged))] public int health = 0;
    [SyncVar(hook = nameof(OnStatusChanged))] public bool alive;
    [SyncVar(hook = nameof(OnAmmoChanged))] public int ammo = 0;
    [SyncVar] public int kill = 0;
    [SyncVar] public int death = 0;
    public int maxHealth = 10;
    public int maxAmmo = 20;

    // Those variables are made to be manipulated only by the server.
    private float respawnTime = 2;
    private float respawnTimeTemp = 0;
    [SyncVar] public bool canRespawn;


    [Header("Components")]
    public PlayerHUDController playerHUDController;
    public PlayerController playerController;
    public PlayerVisualsController playerVisualsController;

    public float TimeBeforeRespawn
    {
        get { return Mathf.Round(respawnTime - respawnTimeTemp); }
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        GameObject infosMultiplayerGO = GameObject.FindGameObjectWithTag("InfoMulti");
        if (infosMultiplayerGO != null)
        {
            if (!infosMultiplayerGO.GetComponent<InfosMultiplayer>().pseudo.Equals(""))
            {
                CmdChangePseudo(infosMultiplayerGO.GetComponent<InfosMultiplayer>().pseudo);
                name = pseudo;
                return;
            }
        }
        CmdChangePseudo("Player n°" + Random.Range(1, 1000));
    }

    public void Update()
    {
        if (!canRespawn)
        {
            if (respawnTimeTemp < respawnTime)
                respawnTimeTemp += Time.deltaTime;
            if (respawnTimeTemp >= respawnTime)
            {
                if (isServer)// We do that here so only the server can pass the boolean to true
                    canRespawn = true;
                respawnTimeTemp = 0;
            } 
        }

    }

    public void OnHealthChanged(int value)
    {
        if(isLocalPlayer)
            playerHUDController.setHP(value);
    }

    public void OnAmmoChanged(int value)
    {
        if (isLocalPlayer)
            playerHUDController.setAmmo(value);
    }

    public void OnPseudoChanged(string newPseudo)
    {
        playerHUDController.setNamePlayer(newPseudo);
        name = newPseudo;
    }

    public void OnStatusChanged(bool isAlive)
    {
        playerVisualsController.playerBody.SetActive(isAlive);
        playerVisualsController.setFlagColor();
        playerVisualsController.rigidB.isKinematic = !isAlive;
        if (isLocalPlayer)
        {
            playerHUDController.alivePanel.SetActive(isAlive);
            playerVisualsController.aliveCam.SetActive(isAlive);

            playerHUDController.deathPanel.SetActive(!isAlive);
            playerVisualsController.deathCam.SetActive(!isAlive);
        }
    }

    public string ColorizedName()
    {
        return ColorizedName(this);
    }

    public string ColorizedName(PlayerInformations player)
    {
        string playerName = player.pseudo;
        if (player.team == Team.RED)
        {
            playerName = "<color=\"red\">" + playerName + "</color>";
        }
        if (player.team == Team.BLUE)
        {
            playerName = "<color=\"blue\">" + playerName + "</color>";
        }
        if (player.team == Team.OBSERVER)
        {
            playerName = "<color=\"black\">" + playerName + "</color>";
        }
        return playerName;
    }

    [Server]
    public void TakeDamages(int value, PlayerInformations player)
    {
        health -= value;
        if (health <= 0)
        {
            health = 0;
            alive = false;
            canRespawn = false;
            respawnTimeTemp = 0;
            Die(player, this == player);
            return;
        }
        ChatAndLogManager.singleton.Logger("Damage from [" + player.pseudo + "] on [" + pseudo + "]");
    }

    [Server]
    private void Die(PlayerInformations player, bool suicide)
    {
        if (SuperGameManager.singleton.gameStatus == GameStatus.GAME)
        {
            if (suicide)
            {
                ChatAndLogManager.singleton.Logger("Oh... " + pseudo + " commited suicide...");
                ChatAndLogManager.singleton.KillAdded(ColorizedName(this), ColorizedName(this));
            }
            else
            {
                ChatAndLogManager.singleton.Logger("[" + player.pseudo + "] killed " + pseudo);
                ChatAndLogManager.singleton.KillAdded(ColorizedName(player), ColorizedName(this));
                player.kill += 1;
            }
            death += 1;
            SuperGameManager.singleton.RemoveTicket(team);
        }
        playerVisualsController.CmdSpawnCadaver();
        playerController.RpcMoveTheDead();
    }

    [Server]
    public void Heal(int value)
    {
        health += value;
        if(health > maxHealth)
        {
            health = maxHealth;
        }
    }

    [Server]
    public void Reload(int value)
    {
        ammo += value;
        if(ammo > maxAmmo)
        {
            ammo = maxAmmo;
        }
    }

    [Server]
    public void ResetStats()
    {
        CmdResetStats();
    }

    [Command]
    public void CmdResetStats()
    {
        kill = death = 0;
    }

    [Command]
    public void CmdChangePseudo(string newPseudo)
    {
        pseudo = newPseudo;
        name = pseudo;
        ChatAndLogManager.singleton.AddLog(newPseudo + " joined the game");
    }

    [Command]
    public void CmdSetInformationTeam(Team team)
    {
        this.team = team;
    }

    [Command]
    public void CmdInformationAtSpawn()
    {
        if (team == Team.OBSERVER)
        {
            this.health = -1;
            return;
        }
        this.health = maxHealth;
        this.ammo = maxAmmo;
        alive = true;
    }
}
