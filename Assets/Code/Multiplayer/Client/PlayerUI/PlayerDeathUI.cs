﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerDeathUI : MonoBehaviour
{

    public PlayerController playerController;

    public Button spawnRedButton;
    public Button spawnBlueButton;
    public Button respawnButton;
    public TextMeshProUGUI respawnButtonLabel;
    public Button changeTeamButton;

    private void OnEnable()
    {
        UpdateUI();
    }

    private void Update()
    {
        if (!playerController.isLocalPlayer)
            return;

        UpdateUI();

    }

    private void UpdateUI()
    {
        if (!playerController.isLocalPlayer)
            return;
        if (playerController.playerInformations.team == Team.OBSERVER)
        {
            respawnButton.gameObject.SetActive(false);
            changeTeamButton.gameObject.SetActive(false);
            spawnBlueButton.gameObject.SetActive(true);
            spawnRedButton.gameObject.SetActive(true);
            spawnBlueButton.interactable = SuperGameManager.singleton.CanSpawnBlue();
            spawnRedButton.interactable = SuperGameManager.singleton.CanSpawnRed();
        }
        else
        {
            respawnButton.gameObject.SetActive(true);
            respawnButton.interactable = playerController.playerInformations.canRespawn;
            if (playerController.playerInformations.canRespawn)
            {
                respawnButtonLabel.text = "RESPAWN";
            }
            else
            {
                respawnButtonLabel.text = "RESPAWN IN "+playerController.playerInformations.TimeBeforeRespawn+"s";
            }
            changeTeamButton.gameObject.SetActive(true);
            spawnBlueButton.gameObject.SetActive(false);
            spawnRedButton.gameObject.SetActive(false);
        }
    }

    public void ClickRespawn()
    {
        playerController.CmdSpawn();
        respawnButton.interactable = false;
    }

    public void ClickChangeTeam()
    {
        teamSelected(Team.OBSERVER);
    }

    public void ClickSelectBlueTeam()
    {
        teamSelected(Team.BLUE);
    }

    public void ClickSelectRedTeam()
    {
        teamSelected(Team.RED);
    }

    public void ClickDisconnect()
    {
        GameObject networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        GameObject infoMulti = GameObject.FindGameObjectWithTag("InfoMulti");
        networkManager.GetComponent<SuperNetworkHUD>().Stop();
        Destroy(networkManager);
        Destroy(infoMulti);
        SceneManager.LoadScene(0);
    }

    private void teamSelected(Team team)
    {
        playerController.playerInformations.CmdSetInformationTeam(team);
        UpdateUI();
    }
}
