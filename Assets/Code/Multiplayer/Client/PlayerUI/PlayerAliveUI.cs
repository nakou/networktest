﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerAliveUI : MonoBehaviour
{

    public PlayerController playerController;

    [Header("Player Stats")]
    public TextMeshProUGUI pseudo;
    public Image healthBar;
    public TextMeshProUGUI health;
    public TextMeshProUGUI ammo;
    public Image reloadingCircle;
    public Image sight;

    public void Update()
    {
        if (!playerController.isLocalPlayer)
            return;
        if (playerController.canFire)
        {
            reloadingCircle.gameObject.SetActive(false);
            sight.gameObject.SetActive(true);
        }
        else
        {
            reloadingCircle.gameObject.SetActive(true);
            reloadAnimation();
            sight.gameObject.SetActive(false);
        }
    }

    public void HealthChanges(int value)
    {
        health.text = value + " HP";
        float val = (float)value / playerController.playerInformations.maxHealth;
        healthBar.fillAmount = val;
    }

    public void PseudoChanges(string value)
    {
        pseudo.text = value;
    }

    public void AmmoChanges(int ammo)
    {
        this.ammo.text = "" + ammo + "/" + playerController.playerInformations.maxAmmo;
    }

    public void reloadAnimation()
    {
        reloadingCircle.fillAmount = playerController.Reload;
    }

}
