﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerEndgameUI : MonoBehaviour
{
    public void Disconnect()
    {
        GameObject networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        GameObject infoMulti = GameObject.FindGameObjectWithTag("InfoMulti");
        networkManager.GetComponent<SuperNetworkHUD>().Stop();
        Destroy(networkManager);
        Destroy(infoMulti);
        SceneManager.LoadScene(0);
    }
}
