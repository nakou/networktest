﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class Bullet : NetworkBehaviour
{
    public float destroyAfter = 5;
    public float ejectionForce = 5000;

    public PlayerInformations origin;
    public Vector3 forceEjectionDirection;

    public PlayerInformations hit;
    public Rigidbody rigidB;

    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), destroyAfter);
    }

    void Start()
    {
        rigidB.AddForce(forceEjectionDirection * ejectionForce);
    }

    // destroy for everyone on the server
    [Server]
    void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }

    // ServerCallback because we don't want a warning if OnTriggerEnter is called on the client
    [ServerCallback]
    private void OnTriggerEnter(Collider other)
    {
        ColisionDetected(other.gameObject);
    }

    void ColisionDetected(GameObject go)
    {
        PlayerInformations informations = go.GetComponentInParent<PlayerInformations>();
        if (informations != null)
        {
            if (informations == origin)
            {
                return;
            }
            if (hit == null)
            {
                hit = informations;
            }
            else
            {
                return;
            }
            informations.TakeDamages(2, origin);
        }
        DestroySelf();
    }
}
