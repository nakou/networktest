﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.EventSystems;

public class PlayerController : NetworkBehaviour
{
    float horizontal = 0;
    float vertical = 0;
    float headRotation = 0;
    bool fire = false;
    public float moveSpeed = 10;

    // Those variables are made to be manipulated only by the server.
    private float fireRate = 2;
    private float reloadTemp = 0;
    [SyncVar(hook = nameof(OnCanFireChanged))] public bool canFire;

    [Header("Components")]
    public PlayerHUDController playerHUDController;
    public PlayerInformations playerInformations;
    public PlayerVisualsController playerVisualsController;

    [Header("Sub-Components")]
    public GameObject bullet;
    public Transform barrel;
    public Transform head;

    public float Reload
    {
        get { return reloadTemp / fireRate; }
    }

    /**
       * This is called when the object is instantiated in the scene "localy".
       **/
    void Start()
    {
        Debug.Log("StartLocal");
        SuperGameManager.singleton.AddPlayer(gameObject);
    }

    public override void OnStartLocalPlayer()
    {
        Debug.Log("OnStartLocalPlayer");
        MoveTheDead();
    }

    /**
     * This is called when the object is destroyed, so the server can delete the player from the list of connected players.
     **/
    private void OnDestroy()
    {
        Debug.LogWarning("DO ME OnDestroy");
        SuperGameManager.singleton.RemovePlayer(gameObject);
    }

    /**
     * Classic Unity Update Loop
     **/
    void Update()
    {
        if (isLocalPlayer)
        {
            if(SuperGameManager.singleton.gameStatus == GameStatus.ENDGAME)
            {
                playerHUDController.EndgameEvent();
                return;
            }
            if (playerInformations.alive)
                ControlsTank();
            Controls();
        }
        if (!canFire)
        {
            if (reloadTemp < fireRate)
                reloadTemp += Time.deltaTime;
            if (reloadTemp >= fireRate && isServer) // We do that here so only the server can pass the boolean to true
                canFire = true;
        }  
    }

    /**
     * Classic Unity FixedUpdate Loop
     **/
    void FixedUpdate()
    {
        if (isLocalPlayer && playerInformations.alive && SuperGameManager.singleton.gameStatus != GameStatus.ENDGAME)
        {
            transform.Translate(new Vector3(0, 0, vertical * moveSpeed));
            transform.Rotate(new Vector3(0, horizontal * moveSpeed * 4, 0));
            head.transform.Rotate(new Vector3(0, headRotation * moveSpeed, 0));

            if (fire && canFire && playerInformations.ammo > 0)
                CmdShoot();
        }
    }

    void OnCanFireChanged(bool value)
    {
        if (!value)
        {
            reloadTemp = 0;
        }
    }

    public void spawnPlayer()
    {
        Transform spawnPosition = null;

        if (playerInformations.team == Team.RED)
            spawnPosition = NetworkManager.singleton.GetComponent<SuperNetworkManager>().redTeamSpawnPosition;

        if (playerInformations.team == Team.BLUE)
            spawnPosition = NetworkManager.singleton.GetComponent<SuperNetworkManager>().blueTeamSpawnPosition;

        if (spawnPosition == null)
        {
            Debug.LogError("Spawn position null : issue with determining team to spawn in!");
            return;
        }

        transform.position = spawnPosition.position;
        alteredSpawnPosition(transform);
    }

    private void alteredSpawnPosition(Transform initialPosition)
    {
        float newX = initialPosition.position.x;
        float newY = initialPosition.position.y;
        float newZ = initialPosition.position.z;
        newX += Random.Range(-3, 3) * 3;
        newZ += Random.Range(-3, 3) * 3;

        initialPosition.position = new Vector3(newX, newY, newZ);
    }

    public void Suicide()
    {
        if(playerInformations.alive)
            CmdSuicide();
    }

    public void MoveTheDead()
    {
        transform.position = SuperGameManager.singleton.deadsPosition.position;
    }

    // ======================================
    // ------------- COMMANDS ---------------
    // ======================================

    /**
     * Send the instruction to the server to kill the player.
     **/
    [Command]
    public void CmdSuicide()
    {
        playerInformations.TakeDamages(100, playerInformations);
    }

    [Command]
    void CmdShoot()
    {
        canFire = false;
        playerInformations.ammo--;
        playerVisualsController.Shoot = true;
        GameObject b = Instantiate(bullet, barrel.position, barrel.rotation);
        b.GetComponent<Bullet>().origin = playerInformations;
        b.GetComponent<Bullet>().forceEjectionDirection = barrel.forward;
        NetworkServer.Spawn(b);
        ChatAndLogManager.singleton.Logger("Shot fired by " + playerInformations.pseudo + "!");
    }

    /**
     * Send the instruction to the server to spawn the player with health, good team information and spawn position.
     **/
    [Command]
    public void CmdSpawn()
    {
        playerInformations.CmdInformationAtSpawn();
        RpcSpawn();
    }

    // ======================================
    // --------------- RPCS -----------------
    // ======================================

    [ClientRpc]
    public void RpcSpawn()
    {
        spawnPlayer();
    }

    [ClientRpc]
    public void RpcMoveTheDead()
    {
        MoveTheDead();
    }

    // ======================================
    // --------- CONTROLS AND STUFF ---------
    // ======================================

    void ControlsTank()
    {
        if (playerHUDController.playerChatUI.inputText.isFocused)
        {
            horizontal = 0;
            vertical = 0;
            headRotation = 0;
            fire = false;
            return;
        }
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        headRotation = Input.GetAxis("Mouse X");
        fire = Input.GetButtonDown("Fire1") && !EventSystem.current.IsPointerOverGameObject();
    }

    void Controls()
    {
        playerHUDController.showPlayerList(Input.GetButton("Playerlist"));
        playerHUDController.playerLogUI.ShowLog(Input.GetButton("DebugInfos"));
        if (Input.GetButtonDown("Submit"))
        {
            playerHUDController.OnChatKeyDown();
        }
        if (Input.GetButtonDown("Cancel"))
            playerHUDController.ingameMenu.SetActive(true);
    }
}
