﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreboardPlayerSlotUI : MonoBehaviour
{
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI kills;
    public TextMeshProUGUI death;
}
