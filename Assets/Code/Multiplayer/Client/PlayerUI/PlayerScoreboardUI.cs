﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScoreboardUI : MonoBehaviour
{
    public GameObject redGrid;
    public GameObject blueGrid;

    private void OnEnable()
    {
        foreach(PlayerInformations player in SuperGameManager.singleton.playerList)
        {
            if (player.team == Team.OBSERVER)
                continue;
            updateInfoSlot(player);
            if (player.team == Team.RED)
            {
                player.playerHUDController.personalSlot.transform.SetParent(redGrid.transform);
                player.playerHUDController.personalSlot.SetActive(true);
                continue;
            }
            if (player.team == Team.BLUE)
            {
                player.playerHUDController.personalSlot.transform.SetParent(blueGrid.transform);
                player.playerHUDController.personalSlot.SetActive(true);
                continue;
            }
        }
    }

    private void updateInfoSlot(PlayerInformations player)
    {
        ScoreboardPlayerSlotUI playerSlotUI = player.playerHUDController.personalSlot.GetComponent<ScoreboardPlayerSlotUI>();
        playerSlotUI.playerName.text = player.pseudo;
        playerSlotUI.kills.text = "" + player.kill;
        playerSlotUI.death.text = "" + player.death;
    }
}
