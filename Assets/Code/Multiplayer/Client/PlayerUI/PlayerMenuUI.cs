﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMenuUI : MonoBehaviour
{

    public PlayerController playerController;

    public void Redeploy()
    {
        playerController.Suicide();
        CloseMenu();
    }

    public void Disconnect()
    {
        GameObject networkManager = GameObject.FindGameObjectWithTag("NetworkManager");
        GameObject infoMulti = GameObject.FindGameObjectWithTag("InfoMulti");
        networkManager.GetComponent<SuperNetworkHUD>().Stop();
        Destroy(networkManager);
        Destroy(infoMulti);
        SceneManager.LoadScene(0);
        CloseMenu();
    }

    public void CloseMenu()
    {
        gameObject.SetActive(false);
    }
}
