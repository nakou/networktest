﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System.ComponentModel;

[AddComponentMenu("Network/NetworkManagerHUD")]
[RequireComponent(typeof(NetworkManager))]
[EditorBrowsable(EditorBrowsableState.Never)]
[HelpURL("https://vis2k.github.io/Mirror/Components/NetworkManagerHUD")]
public class SuperNetworkHUD : MonoBehaviour
{
    SuperNetworkManager manager;
    InfosMultiplayer infosMultiplayer;
    public bool showGUI = true;
    public int offsetX;
    public int offsetY;

    void Awake()
    {
        manager = GetComponent<SuperNetworkManager>();
        GameObject infosMultiplayerGO = GameObject.FindGameObjectWithTag("InfoMulti");
        if(infosMultiplayerGO != null)
        {
            infosMultiplayer = infosMultiplayerGO.GetComponent<InfosMultiplayer>();
            showGUI = false;
        }
    }

    private void Start()
    {
        if(infosMultiplayer == null)
        {
            return;
        }
        switch (infosMultiplayer.mode)
        {
            case MultiMode.HOST:
                StartHeadless();
                break;
            case MultiMode.HOST_AS_PLAYER:
                StartAsHost();
                break;
            case MultiMode.PLAYER:
                StartAsClient();
                break;
        }
    }

    void StartHeadless()
    {
        manager.isServer = true;
        manager.StartHost();
    }

    void StartAsHost()
    {
        manager.isServer = false;
        manager.StartHost();
    }

    void StartAsClient()
    {
        manager.networkAddress = infosMultiplayer.address;
        manager.StartClient();
    }

    public void Stop()
    {
        if (NetworkServer.active || NetworkClient.isConnected)
        {
            manager.StopHost();
        }
    }

    void OnGUI()
    {
        if (!showGUI)
            return;

        GUILayout.BeginArea(new Rect(10 + offsetX, 40 + offsetY, 250, 9999));
        if (!NetworkClient.isConnected && !NetworkServer.active)
        {
            if (!NetworkClient.active)
            {
                // LAN Host
                if (Application.platform != RuntimePlatform.WebGLPlayer)
                {
                    if (GUILayout.Button("LAN Host"))
                    {
                        StartHeadless();
                    }

                    if (GUILayout.Button("LAN Host as Player [ONLY IN DEV]"))
                    {
                        StartAsHost();
                    }
                }

                // LAN Client + IP
                GUILayout.BeginHorizontal();
                if (GUILayout.Button("LAN Client"))
                {
                    manager.StartClient();
                }
                manager.networkAddress = GUILayout.TextField(manager.networkAddress);
                GUILayout.EndHorizontal();

                // LAN Server Only
                if (Application.platform == RuntimePlatform.WebGLPlayer)
                {
                    // cant be a server in webgl build
                    GUILayout.Box("(  WebGL cannot be server  )");
                }
                else
                {
                    if (GUILayout.Button("LAN Server Only")) manager.StartServer();
                }
            }
            else
            {
                // Connecting
                GUILayout.Label("Connecting to " + manager.networkAddress + "..");
                if (GUILayout.Button("Cancel Connection Attempt"))
                {
                    manager.StopClient();
                }
            }
        }
        else
        {
            // server / client status message
            if (NetworkServer.active)
            {
                GUILayout.Label("Server: active. Transport: " + Transport.activeTransport);
            }
            if (NetworkClient.isConnected)
            {
                GUILayout.Label("Client: address=" + manager.networkAddress);
            }
        }

        // client ready
        if (NetworkClient.isConnected && !ClientScene.ready)
        {
            if (GUILayout.Button("Client Ready"))
            {
                ClientScene.Ready(NetworkClient.connection);

                if (ClientScene.localPlayer == null)
                {
                    ClientScene.AddPlayer();
                }
            }
        }

        // stop
        if (NetworkServer.active || NetworkClient.isConnected)
        {
            if (GUILayout.Button("Stop"))
            {
                manager.StopHost();
            }
        }

        GUILayout.EndArea();
    }
}
