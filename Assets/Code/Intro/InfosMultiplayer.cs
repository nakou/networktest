﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfosMultiplayer : MonoBehaviour
{
    private static InfosMultiplayer instance;

    public static InfosMultiplayer Instance { get { return instance; } }

    public string address = "";

    public MultiMode mode = MultiMode.PLAYER;

    public string pseudo = ""; 

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
}
