﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using UnityEngine.UI;

/**
 * The Server Player is a special "Host" player that spawn instead of a normal player prefab when you start the game in "Host" mode. 
 * The idea behind is to have a Server-only player that can't play.
 * It's just a security mesure : in UNET, the server is a god that has access to everything. Having a player with that power is not good if you want
 * your game to have a competitive mod (hacking).
 * 
 * You can imagine a situation when the server.exe is deployed on a gamedev controlled server only and player connect themselves on it.
 **/
public class ServerPlayer : NetworkBehaviour
{
    public GameObject cam;
    public GameObject canvas;
    public TextMeshProUGUI listPlayer;
    public TextMeshProUGUI logs;
    public TextMeshProUGUI timer;
    public TextMeshProUGUI redTickets;
    public TextMeshProUGUI blueTickets;

    public static ServerPlayer singleton;

    private float refreshTimer = 1;
    private float refreshTimerTemp = 0;

    public void Awake()
    {
        InitializeSingleton();
        listPlayer.text = "";
        timer.text = "";
        blueTickets.text = "";
        redTickets.text = "";
        logs.text = "";
    }

    void InitializeSingleton()
    {
        Debug.Log("Initialize Singleton");
        if (singleton != null && singleton == this)
        {
            return;
        }
        singleton = this;
    }

    public override void OnStartServer()
    {
        cam.SetActive(true);
        canvas.SetActive(true);
    }

    void Update()
    {
        if (!isServer)
            return;
        if(refreshTimerTemp >= refreshTimer)
        {
            refreshDatas();
            refreshTimerTemp = 0;
        }
        refreshTimerTemp += Time.deltaTime;
    }

    void refreshDatas()
    {
        timer.text = Mathf.Round(SuperGameManager.singleton.GetTimeLeft) + "s";
        blueTickets.text = SuperGameManager.singleton.blueTickets + "";
        redTickets.text = SuperGameManager.singleton.redTickets + "";
        listPlayer.text = "<color=\"blue\">" + SuperGameManager.singleton.printBlueList() + "</color>";
        listPlayer.text += "<color=\"red\">" + SuperGameManager.singleton.printRedList() + "</color>";
    }

    public void AddLog(string log)
    {
        logs.text += "\n" + log;
    }
}
