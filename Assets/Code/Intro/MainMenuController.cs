﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    [Header("Technical")]
    public InfosMultiplayer infosMultiplayer;

    [Header("Menus")]
    public GameObject menuCanvas;
    public GameObject connectMenuCanvas;
    public TMP_InputField inputIP;
    public TMP_InputField inputPseudo;

    [Header("Camera")]
    public float speed = 1;
    public Transform menuPosition;
    public Transform menuLookat;
    public Transform connectPosition;
    public Transform connectLookat;

    Transform actualPosition;
    Transform actualFocus;
    Transform focusTransform;

    private void Start()
    {
        ClickReturn();
        focusTransform = Instantiate(menuLookat, menuLookat.position, menuLookat.rotation);
    }

    private void Update()
    {
        float step = speed * Time.deltaTime;
        transform.LookAt(focusTransform);
        focusTransform.position = Vector3.MoveTowards(focusTransform.position, actualFocus.position, step / 1.5f);
        transform.position = Vector3.MoveTowards(transform.position, actualPosition.position, step);
    }

    void MoveTo(Transform position, Transform focus)
    {
        actualPosition = position;
        actualFocus = focus;
    }

    public void ClickStartHeadless()
    {
        infosMultiplayer.mode = MultiMode.HOST;
        LoadNextScene();
    }

    public void ClickConnectToServer()
    {
        MoveTo(connectPosition, connectLookat);
        menuCanvas.SetActive(false);
        connectMenuCanvas.SetActive(true);
    }

    public void ClickFinalConnect()
    {
        infosMultiplayer.mode = MultiMode.PLAYER;
        infosMultiplayer.address = inputIP.text;
        if (inputIP.text.Equals(""))
        {
            infosMultiplayer.address = "localhost";
        }
        infosMultiplayer.pseudo = inputPseudo.text;
        LoadNextScene();
    }

    public void ClickStartAsHost()
    {
        infosMultiplayer.mode = MultiMode.HOST_AS_PLAYER;
        LoadNextScene();
    }

    public void ClickQuit()
    {
        Application.Quit();
    }

    public void ClickReturn()
    {
        MoveTo(menuPosition, menuLookat);
        menuCanvas.SetActive(true);
        connectMenuCanvas.SetActive(false);
        inputIP.text = "";
    }

    public void LoadNextScene()
    {
        SceneManager.LoadScene(1);
    }
}
