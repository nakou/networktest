﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Mirror;

public class PlayerLogUI : MonoBehaviour
{
    public TextMeshProUGUI logText;
    LogStack logStack;

    [Header("Configuration")]
    public int logLineLimit;

    public void ShowLog(bool show)
    {
        gameObject.SetActive(show);
        publish();
    }

    private void publish()
    {
        if (gameObject.activeSelf)
        {
            logText.text = logStack.PrintLog();
        }
    }

    public void AddMessage(string message)
    {
        if(logStack == null)
            logStack = new LogStack(logLineLimit);
        logStack.AddLine(message);
        publish();
    }
}
