﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerChatUI : MonoBehaviour
{
    public TextMeshProUGUI logText;
    public TMP_InputField inputText;

    LogStack chatStack;

    [Header("Configuration")]
    public int chatLineLimit;

    public void Awake()
    {
        chatStack = new LogStack(chatLineLimit);
        publish();
    }

    public void ShowChat(bool show)
    {
        gameObject.SetActive(show);
        publish();
    }

    public void publish()
    {
        logText.text = chatStack.PrintLog();
    }

    public void AddMessage(string message)
    {
        if (chatStack == null)
            chatStack = new LogStack(chatLineLimit);
        chatStack.AddLine(message);
        publish();
    }

}
