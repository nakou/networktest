﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Mirror;

public class PlayerHUDController : NetworkBehaviour
{
    [Header("Player Controller")]
    public PlayerController playerController;

    [Header("UI Managers")]
    public GameObject scoreBoardUI;
    public GameObject endgameMenu;
    public GameObject deathPanel;
    public GameObject alivePanel;
    public GameObject gameStatusPanel;
    public GameObject ingameMenu;
    public PlayerLogUI playerLogUI;
    public PlayerChatUI playerChatUI;
    public GameObject personalSlot;

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        deathPanel.SetActive(true);
        gameStatusPanel.SetActive(true);
        playerChatUI.ShowChat(true);
    }

    public void EndgameEvent()
    {
        if (isLocalPlayer)
        {
            deathPanel.SetActive(false);
            alivePanel.SetActive(false);
            ingameMenu.SetActive(false);
            scoreBoardUI.gameObject.SetActive(true);
            endgameMenu.SetActive(true);
        }
    }

    public void setHP(int value)
    {
        alivePanel.GetComponent<PlayerAliveUI>().HealthChanges(value); 
    }

    public void setAmmo(int value)
    {
        alivePanel.GetComponent<PlayerAliveUI>().AmmoChanges(value);
    }

    public void showPlayerList(bool show)
    {
        scoreBoardUI.gameObject.SetActive(show);
    }

    public void showDebug(bool show)
    {
        playerLogUI.ShowLog(show);
    }

    public void setNamePlayer(string playerName)
    {
        alivePanel.GetComponent<PlayerAliveUI>().PseudoChanges(playerName);
    }

    public void OnChatKeyDown()
    {
        if (playerChatUI.inputText.gameObject.activeSelf)
        {
            SendChatMessage();
        }
        else
        {
            playerChatUI.inputText.gameObject.SetActive(true);
            playerChatUI.inputText.Select();
            playerChatUI.inputText.ActivateInputField();
        }
    }

    public void SendChatMessage()
    {
        playerChatUI.inputText.ReleaseSelection();
        playerChatUI.inputText.gameObject.SetActive(false);
        string playerName = playerController.playerInformations.ColorizedName();
        string input = playerChatUI.inputText.text;
        if (!playerController.playerInformations.alive)
        {
            playerName = "(Dead)" + playerName;
        }
        if (!input.Equals(""))
        {
            if (!checkForCommand(input))
            {
                CmdSendChatMessage(playerName, input);
            }
            playerChatUI.inputText.text = "";
        }
    }

    private bool checkForCommand(string input)
    {
        if(input[0] == '/')
        {
            if (input.Equals("/suicide") && playerController.playerInformations.alive)
            {
                playerController.CmdSuicide();
            }
            return true;
        } else
        {
            return false; 
        }
    }

    public void UpdateChat(string value)
    {
        playerChatUI.AddMessage(value);
    }

    public void UpdateKill(string value)
    {
        gameStatusPanel.GetComponent<PlayerGameStatusUI>().AddKill(value);
    }

    public void UpdateLog(string value)
    {
        playerLogUI.AddMessage(value);
    }

    /*
     * Because we are modifying a SyncVar, we must pass that as a Command. And the command is in the PlayerHUD cause the player
     * has the authority to do so.
     * */
    [Command]
    public void CmdSendChatMessage(string playerName, string input)
    {
        ChatAndLogManager.singleton.ChatAdded(playerName, input);
    }

    [Command]
    public void CmdSendLogMessage(string log)
    {
        ChatAndLogManager.singleton.Logger(log);
    }
}
