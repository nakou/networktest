﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;


/**
 * This object is a singleton which will manage :
 * A shared log system
 * Chat datas
 * Player list
 * Ticket list
 * Game Timer
 * 
 * It's a singleton because :
 * - it should be only one per client. Some of its data is synchronised between the players, other datas are basically the same,
 * except that it's the player who reconstruct those value with datas based on client (playerList, matchTimer for example).
 * - making it a singleton makes it very easy to access it everywhere.
 * 
 * An explaination of the SuperGameManager :
 * 
 * 1 - Server-side decisions :
 * You'll notice that a lot of methods of this class are used only if [isServer = true]. That's because we want only the SuperGameManager 
 * of the server to execute "master"/"autoritay" instructions, like verify if the game can start, timer, ticket count and so on.
 * Those variable are so "protected" from external modifications, since no internal [Command] can be accessed by another Player object.
 * But since those values are SyncVar, they are synchronized between client, which mean they can be read (but not modified) by the clients.
 * 
 * The mechanic inside those method are not complicated and are mostly gameplay-driven.
 * 
 * 2 - Local datas :
 * Some of the datas, like the PlayerList and localPlayer are local. Therefore they will be "different" on each client.
 * But in the case of the PlayerList, since they are constructed the same way on every client, even if the object itself is not shared,
 * the element composing the list are sharing syncvar in them that are synchronized between clients.
 * 
 * The localPlayer, however, is unique because it's the Player of the client it's played on. This allow to call methods on those clients
 * directly (which themselve call [Command]s and so on).
 * 
 **/
public class SuperGameManager : NetworkBehaviour
{
    public List<PlayerInformations> playerList = new List<PlayerInformations>();
    public PlayerInformations localPlayer;

    public static SuperGameManager singleton;
    [Header("Game Objects")]
    public Transform deadsPosition;
    [Header("Shared Informations")]
    [SyncVar] public int redTickets;
    [SyncVar] public int blueTickets;
    [SyncVar] public GameStatus gameStatus = GameStatus.WARMUP;

    [Header("Game Setup")]
    [SyncVar] public int baseTickets = 100;
    [SyncVar] public float baseGameTimer = 120;
    public int minimalPlayerCountPerTeamToStart = 1;

    [Header("Local Informations")]
    [SyncVar] private float gameTimer;
    [SyncVar] private float currentGameTimer = 0;

    //Server Only Variables
    private float checkGameStateTimer = 2;
    private float checkGameStateTimerTemp = 0;

    public float GetTimeLeft
    {
        get { return gameTimer - currentGameTimer; }
    }

    public void Awake()
    {
        InitializeSingleton();
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        InitializeGameDatas();
    }

    private void InitializeGameDatas()
    {
        baseTickets = 5; // For debugging purposses
        redTickets = blueTickets = baseTickets;
        gameTimer = baseGameTimer;
    }

    public void Update()
    {
        if (!isServer)
            return;
        ManageGameStatusOnServer();
        if(gameStatus == GameStatus.GAME)
        {
            currentGameTimer += Time.deltaTime;
        }
    }

    private void ManageGameStatusOnServer()
    {
        if (!isServer)
            return;
        if (checkGameStateTimerTemp < checkGameStateTimer)
        {
            checkGameStateTimerTemp += Time.deltaTime;
            return;
        }
        checkGameStateTimerTemp = 0; // if we are here, we are on server, and it's time to check how the game is going
        if (gameStatus == GameStatus.WARMUP && canStartGame()) // can the match start?
        {
            gameStatus = GameStatus.GAME;
            KillEveryOneAndResetStats();
            return;
        }
        if(gameStatus == GameStatus.GAME && isGameOver()) // can the match end?
        {
            gameStatus = GameStatus.ENDGAME;
        }
    }

    private bool isGameOver()
    {
        bool timeOut = currentGameTimer > gameTimer;
        bool blueTeamLost = blueTickets <= 0;
        bool redTeamLost = redTickets <= 0;
        return timeOut || blueTeamLost || redTeamLost;
    }

    private bool canStartGame()
    {
        if(playerList.Count >= minimalPlayerCountPerTeamToStart * 2)
        {
            if (getTeamPlayerList(Team.RED).Count >= minimalPlayerCountPerTeamToStart && getTeamPlayerList(Team.BLUE).Count >= minimalPlayerCountPerTeamToStart)
            {
                return true;
            }
        }
        return false;
    }

    private void KillEveryOneAndResetStats()
    {
        RpcKillLocalPlayerAndReset();
        InitializeGameDatas();
        ChatAndLogManager.singleton.Logger("The match is starting!");
    }

    [ClientRpc]
    private void RpcKillLocalPlayerAndReset()
    {
        if (isServer)
            return;
        localPlayer.playerController.Suicide();
        localPlayer.ResetStats();
    }

    void InitializeSingleton()
    {
        Debug.Log("Initialize Singleton");
        if (singleton != null && singleton == this)
        {
            return;
        }
        singleton = this;
    }

    public void RemoveTicket(Team team)
    {
        if(team == Team.BLUE)
        {
            blueTickets--;
        }
        if (team == Team.RED)
        {
            redTickets--;
        }
    }

    public bool CanSpawnRed()
    {
        return getTeamPlayerList(Team.RED).Count <= getTeamPlayerList(Team.BLUE).Count;
    }

    public bool CanSpawnBlue()
    {
        return getTeamPlayerList(Team.BLUE).Count <= getTeamPlayerList(Team.RED).Count;
    }

    public void AddPlayer(GameObject playerObj)
    {
        PlayerInformations player = playerObj.GetComponent<PlayerInformations>();
        playerList.Add(player);
        if (player.isLocalPlayer)
        {
            localPlayer = player;
        }
    }

    public void RemovePlayer(GameObject playerObj)
    {
        PlayerInformations player = playerObj.GetComponent<PlayerInformations>();
        if (isServer)
            ChatAndLogManager.singleton.AddLog(player.pseudo + " left the game");
        if (!player.isLocalPlayer)
        {
            playerList.Remove(player.GetComponent<PlayerInformations>());
        }
    }

    public List<PlayerInformations> getTeamPlayerList(Team team)
    {
        List<PlayerInformations> list = new List<PlayerInformations>();
        foreach (PlayerInformations playerInfo in playerList)
        {
            if (playerInfo.team == team)
            {
                list.Add(playerInfo);
            }
        }
        return list;
    }

    public string printRedList()
    {
        string list = "";
        foreach (PlayerInformations pC in playerList)
        {
            if(pC.team == Team.RED)
            {
                list += "\n " + pC.pseudo;
            }
        }
        return list;
    }

    public string printBlueList()
    {
        string list = "";
        foreach (PlayerInformations pC in playerList)
        {
            if (pC.team == Team.BLUE)
            {
                list += "\n " + pC.pseudo;
            }
        }
        return list;
    }
}
