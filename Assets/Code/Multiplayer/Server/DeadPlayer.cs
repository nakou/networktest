﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class DeadPlayer : NetworkBehaviour
{

    private float destroyAfter = 3;

    public override void OnStartServer()
    {
        Invoke(nameof(DestroySelf), destroyAfter);
    }

    [Server]
    void DestroySelf()
    {
        NetworkServer.Destroy(gameObject);
    }
}
