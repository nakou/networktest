
# MiniTank, Through the looking Glass
### A mini, hopefully educational project powered by Mirror

## Introduction

This project named poorly "MiniTank" is supposed to be a Unity example project based on the Mirror implementation of the multiplayer in Unity3D.
I aimed to make a comprehensive, simple yet complete project which can be used as blueprint for a TPS/FPS multiplayer game.

Keep in mind that it was a project for me to learn, and to share my discoveries with you.
Don't hesitate to comment and PR the code if you have better implementations and ideas.

## The "Game"

The game is a simple TPS with little Tanks. It's a deathmatch between Blue team and Red team.

## Current status
The current status of the project is Working! Yay! (Thanks to Rewar and MrGadget)
I am still working through my Todo list.

## Current Features
- Fake-headless server. It's a server that is not a player (I won't implement a true bashmode version since it's not the goal of this project).
- Player List (Press Tab)
- Chat
- Console logs (Press End)
- Respawn system
- Team selection (with limitation if a team has less people in it)
- TDM mode
- Warmup -> Match -> Endgame cycle.

### About the code architecture

So, basically I've tried to separate a lot of things to be as clear as possible.
Sometimes it's worth it, sometimes it's not.

Due to the intricate Server/Client relation in Mirror projects, I **HIGHLY RECOMMEND** that you read the Mirror [Documentation](https://mirror-networking.com/docs/) first. You'll find comments and explanations, but some of them will makes no sense if you don't know some basis in Mirror and Networking in Unity.

So let's dive in :

- **Code folder**
  - **Intro folder**: All this code is basic main-menu stuff, not really interesting except...
    - ***InfosMultiplayer.cs*** is an object that will be sent to the main game when launched. It contains all the information to connect to the server gathered in the main menu.
  - **Multiplayer folder** : This is where all the magic is happening.
    - **Client folder** : This is where code will concern the Client prefab set.
      - **PlayerUI folder** : This is where I've placed all the UI management code. Buttons, chat, healthbar, everything is managed here. No classes here is a NetworkBehaviour. Which means that their comportments are mostly managed by...
      - ***PlayerController.cs*** is a class which the main goal is to manage inputs and player actions. It's also the class that manages positions for movements, spawn, and also the class that spawn bullets.
      - ***PlayerHUDController.cs*** is a class that will manage HUD and UIs elements of the player; which one is active and when, gather and send chat messages, update chatbox, kills and shared logs. It's the hub for everything HUD related (duh).
      - ***PlayerInformations.cs*** is a class that manages all the informations of the player. How many health points and ammo left, how many kills, is it alive, what's its name, how many time before respawn, is the kind of question this class is responding to.
      - ***PlayerVisualsController.cs*** is a class that manages all the visuals of the player; i.e. is the player visible to the other, what color is its flag, which camera is it using and so on... it's also the class responsible to spawn dead bodies when a player dies.
    -  **Common folder** : This is where I store things like utility classes, enums, this kind of things.
    -  **Server folder** : This is where the code concerning the server is stored, also with the code for the server prefabs.
	    - **Managers folder** : This where I store the two important classes of the server, which are :
		    - ***SuperGameManager.cs*** is a *Singleton* that contains all the informations about the match : timer, tickets, game actual mode (Warmup, Match, Finished) and list of player. It's also the class that check when and why a match is starting, or is over. Some information are server-side, other are shared, other are locals, but this is one of the important class of the project since it manages a lot of things. This is the most complicated class to understand as a newbie (because even if it exist on client and on server, some of the code is executed only server-side and have consequences on both, and so on).
		    - ***ChatAndLogManager.cs*** is also a Singleton and kinda a light version of SuperGameManager.cs. It's role is to receive informations from the player concerning Chat, kill and shared logs, and send them back to all the player to be added in their respective UIs.
		    - ***Bullet.cs*** is the class that manages Bullets/Shells shot by the player, as their impacts and consequences.
		    - ***DeadPlayer.cs*** is a simple class that destroy a dead body after a short period of time.
		    - ***ItemSpawner.cs*** is a class that manages the items that can appear in the game, their actions and reload timer.
		    - ***ServerPlayer.cs*** is a big and ugly class that manages the Server Prefab.
		    - ***SuperNetworkHUD.cs*** is a modified copy of the NetworkHUD from Mirror that include part that will check if InfosMultiplayer exist and if we need the old ugly HUD or if we can start with pre-selected elements for the server.
		    - ***SuperNetworkManager.cs*** is an extension to NetworkManager from Mirror that overrides and changes the spawn system to include the ServerPrefab in the system (and differentiate the Player and the Server).

### About the architecture

The player that host the game is not a real player. It's just a camera that show the list of connected players.
The players that are classical clients.

So host and players have different prefabs that are instantiated by a different process.

Why do I use a special prefab and system for the host of the game?
Because I want to be able to create a "Server only" executable, that can be hosted in a virtual machine.
In the case of a casual play, it's not really needed, but if you want to have a competitive/ranking system in your game, 
it can become handy, as having a server hosted on a controlled server can help you fight hacking and cheating in your game.

Their is also a "Host as player" mode but I can't vouch for it. The fact is, Host as Player can hide issues and mismanagement of the SyncVar,
so I do think it's a good habit to ignore this mode all together if you don’t need it in your project.

#### About hacking and security

The focus of this project is to give people elements that can help them understand networking in the context of a simple and classic game. Not to make a secured game.

Since Unity games can be unpack and "decompliated", it's really complicated to make a game fully-safe.

Trust me, I tried more than once.

I direct you to take advice on the Mirror discord for that.

#### Log and Chat Stacks

The logs and chat are managed with an home-made stack system that keep only a limited number of messages. It was kind of an experiment into another experiment.

### Unity version and requirements
The project is based on Unity 2019.2.11f1, Mirror 5.0.2 and the project use TextMeshPro.

## Other documentation
Check the Mirror [Documentation](https://mirror-networking.com/docs/) for more information on the "API" used.

### Donations
I've spent something like one and a half month on this little thing. So if it was usefull to you, please consider donate a tip via the [itch.io page of the project](https://nakou.itch.io/minitank-mirror)!


## Maintainer

- Captain Nakou

## Contributor

- MrGadget

### Special Thanks

- MrGadget and the #help of the Mirror Discord for all the help, the PR, the comments and the support they provided.
- Rewar (For helping trying to clean this mess)
- The A-Team (Aelitz, Leinox, Sighne, Baenshee, for help and emotionnal support)
- My love Feilyn, go check her [arts](https://www.instagram.com/feilyn/)!

## Licence

This project is under MIT licence.
Some of the icons are from the [OpenEmoji](https://openmoji.org/about/) and are under Creative Commons [Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license.

