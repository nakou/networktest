﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogStack
{
    private int linesLimit;
    private List<string> logs;

    public LogStack(int linesLimit)
    {
        this.linesLimit = linesLimit;
        logs = new List<string>();
    }

    public void AddLine(string logLine)
    {
        if (logs.Count > linesLimit)
        {
            logs.RemoveAt(0);
        }
        logs.Add(logLine);
    }

    public string PrintLog()
    {
        string retVal = "";
        int counter = 0;
        foreach(string line in logs)
        {
            counter++;
            retVal += line;
            if(logs.Count > counter)
            {
                retVal += "\n";
            }
        }
        return retVal;
    }
}
